// © 2017 - 2020 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

module lpc_master_interface(
		input wire [15:0] address,
		input wire [7:0] tx_data,
		output reg [7:0] rx_data,
		input wire tpm_cycle,
		input wire tx_start,
		input wire rx_start,
		input wire cycle_abort,
		output reg transaction_complete,
		output reg [16:0] irq_status,
		input wire irq_ack,
		input wire irq_poll,
		input wire reset,

		output reg [3:0] lpc_data_out,
		input wire [3:0] lpc_data_in,
		output reg lpc_data_direction,	// 0 == tristate (input), 1 == driven (output)
		output reg lpc_irq_out,
		input wire lpc_irq_in,
		output reg lpc_irq_direction,	// 0 == tristate (input), 1 == driven (output)

		output reg lpc_frame_n,
		output reg lpc_reset_n,
		input wire lpc_clock
	);

	parameter LPC_CODEWORD_ISA_START = 4'b0000;
	parameter LPC_CODEWORD_TPM_START = 4'b0101;

	parameter LPC_CODEWORD_SYNC_READY = 4'b0000;
	parameter LPC_CODEWORD_SYNC_SWAIT = 4'b0101;
	parameter LPC_CODEWORD_SYNC_LWAIT = 4'b0110;
	parameter LPC_CODEWORD_SYNC_ERROR = 4'b1010;
	parameter LPC_CODEWORD_TURNAROUND = 4'b1111;

	parameter LPC_CYCLE_TYPE_IO = 2'b00;

	parameter LPC_TRANSFER_STATE_IDLE = 0;
	parameter LPC_TRANSFER_STATE_TR01 = 1;
	parameter LPC_TRANSFER_STATE_TR02 = 2;
	parameter LPC_TRANSFER_STATE_TR03 = 3;
	parameter LPC_TRANSFER_STATE_TR04 = 4;
	parameter LPC_TRANSFER_STATE_TR05 = 5;
	parameter LPC_TRANSFER_STATE_TR06 = 6;
	parameter LPC_TRANSFER_STATE_TR07 = 7;
	parameter LPC_TRANSFER_STATE_TR08 = 8;
	parameter LPC_TRANSFER_STATE_TR09 = 9;
	parameter LPC_TRANSFER_STATE_TR10 = 10;
	parameter LPC_TRANSFER_STATE_TR11 = 11;
	parameter LPC_TRANSFER_STATE_TR12 = 12;
	parameter LPC_TRANSFER_STATE_TR13 = 13;
	parameter LPC_TRANSFER_STATE_TR14 = 14;
	parameter LPC_TRANSFER_STATE_TR15 = 15;
	parameter LPC_TRANSFER_STATE_TR16 = 16;
	parameter LPC_TRANSFER_STATE_TR17 = 17;
	parameter LPC_TRANSFER_STATE_TR18 = 18;
	parameter LPC_TRANSFER_STATE_TR19 = 19;
	parameter LPC_TRANSFER_STATE_TR20 = 20;
	parameter LPC_TRANSFER_STATE_TR21 = 21;

	parameter LPC_SERIRQ_STATE_IDLE = 0;
	parameter LPC_SERIRQ_STATE_TR01 = 1;
	parameter LPC_SERIRQ_STATE_TR02 = 2;
	parameter LPC_SERIRQ_STATE_TR03 = 3;
	parameter LPC_SERIRQ_STATE_TR04 = 4;

	reg [4:0] transfer_state;
	reg [2:0] serirq_state;
	reg [1:0] cycle_type = 0;
	reg cycle_direction = 0;	// 0 == read, 1 == write
	reg [15:0] io_address = 0;
	reg [7:0] io_data = 0;
	reg [3:0] timeout = 0;
	reg [3:0] irq_delay_counter = 0;
	reg [4:0] irq_frame_number = 0;
	reg irq_ack_prev = 0;
	reg irq_poll_prev = 0;

	reg reset_reg = 0;
	reg tx_start_reg = 0;
	reg rx_start_reg = 0;
	reg cycle_abort_reg = 0;
	reg irq_reset_reg = 0;
	reg irq_ack_reg = 0;
	reg irq_poll_reg = 0;

	always @(posedge lpc_clock) begin
		// Avoid logic glitches due to these signals crossing clock domains
		irq_reset_reg <= reset;
		irq_ack_reg <= irq_ack;
		irq_poll_reg <= irq_poll;

		if (irq_reset_reg) begin
			irq_status <= 0;
			lpc_irq_out <= 0;
			lpc_irq_direction <= 0;
			serirq_state <= LPC_SERIRQ_STATE_IDLE;
		end else begin
			case (serirq_state)
				LPC_SERIRQ_STATE_IDLE: begin
					lpc_irq_direction <= 0;
					if (irq_poll_reg && !irq_poll_prev) begin
						lpc_irq_out <= 0;
						lpc_irq_direction <= 1;
						irq_delay_counter <= 0;
						serirq_state <= LPC_SERIRQ_STATE_TR01;
					end
				end
				LPC_SERIRQ_STATE_TR01: begin
					// Continue driving frame start
					if (irq_delay_counter > 3) begin
						lpc_irq_out <= 1;
						serirq_state <= LPC_SERIRQ_STATE_TR02;
					end else begin
						irq_delay_counter <= irq_delay_counter + 1;
					end
				end
				LPC_SERIRQ_STATE_TR02: begin
					// Tristate line before slave TX start
					lpc_irq_direction <= 0;
					irq_delay_counter <= 0;
					irq_frame_number <= 0;
					serirq_state <= LPC_SERIRQ_STATE_TR03;
				end
				LPC_SERIRQ_STATE_TR03: begin
					if (irq_delay_counter == 0) begin
						if (!lpc_irq_in) begin
							irq_status[irq_frame_number] = 1;
						end
					end

					if (irq_delay_counter > 1) begin
						irq_frame_number <= irq_frame_number + 1;
						irq_delay_counter <= 0;
					end else begin
						irq_delay_counter <= irq_delay_counter + 1;
					end

					if (irq_frame_number > 16) begin
						lpc_irq_out <= 0;
						lpc_irq_direction <= 1;
						irq_delay_counter <= 0;
						serirq_state <= LPC_SERIRQ_STATE_TR04;
					end
				end
				LPC_SERIRQ_STATE_TR04: begin
					// Continue driving frame stop (continuous mode)
					if (irq_delay_counter > 1) begin
						lpc_irq_out <= 1;
						lpc_irq_direction <= 0;
						serirq_state <= LPC_SERIRQ_STATE_IDLE;
					end else begin
						irq_delay_counter <= irq_delay_counter + 1;
					end
				end
			endcase
		end

		if (irq_ack_reg && !irq_ack_prev) begin
			irq_status = 0;
		end
		irq_ack_prev <= irq_ack_reg;
		irq_poll_prev <= irq_poll_reg;
	end

	always @(posedge lpc_clock) begin
		// Avoid logic glitches due to these signals crossing clock domains
		reset_reg <= reset;
		tx_start_reg <= tx_start;
		rx_start_reg <= rx_start;
		cycle_abort_reg <= cycle_abort;

		if (reset_reg) begin
			lpc_frame_n <= 1;
			lpc_reset_n <= 0;
			lpc_data_direction <= 0;
			lpc_data_out <= LPC_CODEWORD_TURNAROUND;

			transaction_complete <= 0;
			transfer_state <= LPC_TRANSFER_STATE_IDLE;
		end else begin
			case (transfer_state)
				LPC_TRANSFER_STATE_IDLE: begin
					// Idle state
					cycle_type = 0;
					cycle_direction = 0;
					io_address <= 0;
					timeout <= 0;
					transaction_complete <= 0;
					if ((tx_start_reg) || (rx_start_reg)) begin
						io_address <= address;
						io_data <= tx_data;
						transfer_state <= LPC_TRANSFER_STATE_TR01;
					end else begin
						transfer_state <= LPC_TRANSFER_STATE_IDLE;
					end

					lpc_frame_n <= 1;
					lpc_data_direction <= 0;
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
				end
				LPC_TRANSFER_STATE_TR01: begin
					// Drive frame start
					if (tpm_cycle) begin
						lpc_data_out <= LPC_CODEWORD_TPM_START;
					end else begin
						lpc_data_out <= LPC_CODEWORD_ISA_START;
					end
					lpc_frame_n <= 0;
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR02;
				end
				LPC_TRANSFER_STATE_TR02: begin
					// Drive cycle type and direction
					lpc_data_out[3:2] <= LPC_CYCLE_TYPE_IO;
					lpc_data_out[1] <= tx_start_reg;
					lpc_data_out[0] <= 1'b0;
					lpc_frame_n <= 1;
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR03;
				end
				LPC_TRANSFER_STATE_TR03: begin
					// Transmit I/O address -- nibble 1
					lpc_data_out <= io_address[15:12];
					transfer_state <= LPC_TRANSFER_STATE_TR04;
				end
				LPC_TRANSFER_STATE_TR04: begin
					// Transmit I/O address -- nibble 2
					lpc_data_out <= io_address[11:8];
					transfer_state <= LPC_TRANSFER_STATE_TR05;
				end
				LPC_TRANSFER_STATE_TR05: begin
					// Transmit I/O address -- nibble 3
					lpc_data_out <= io_address[7:4];
					transfer_state <= LPC_TRANSFER_STATE_TR06;
				end
				LPC_TRANSFER_STATE_TR06: begin
					// Transmit I/O address -- nibble 4
					lpc_data_out <= io_address[3:0];

					// Set up data transfer
					if (rx_start_reg) begin
						transfer_state <= LPC_TRANSFER_STATE_TR09;
					end else begin
						transfer_state <= LPC_TRANSFER_STATE_TR07;
					end
				end
				LPC_TRANSFER_STATE_TR07: begin
					// Transmit I/O data -- nibble 1
					lpc_data_out <= io_data[3:0];
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR08;
				end
				LPC_TRANSFER_STATE_TR08: begin
					// Transmit I/O data -- nibble 2
					lpc_data_out <= io_data[7:4];
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR09;
				end
				LPC_TRANSFER_STATE_TR09: begin
					// Drive turn-around cycle part 1
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR10;
				end
				LPC_TRANSFER_STATE_TR10: begin
					// Drive turn-around cycle part 2
					lpc_data_direction <= 0;
					transfer_state <= LPC_TRANSFER_STATE_TR11;
				end
				LPC_TRANSFER_STATE_TR11: begin
					// Wait for sync ready flag from peripheral
					if (lpc_data_in == LPC_CODEWORD_SYNC_READY) begin
						if (rx_start_reg) begin
							transfer_state <= LPC_TRANSFER_STATE_TR12;
						end else begin
							transfer_state <= LPC_TRANSFER_STATE_TR14;
						end
					end else begin
						if ((cycle_abort_reg) || (timeout > 3)) begin
							transfer_state <= LPC_TRANSFER_STATE_TR16;
						end
					end

					timeout <= timeout + 1;
				end
				LPC_TRANSFER_STATE_TR12: begin
					// Receive I/O data -- nibble 1
					rx_data[3:0] = lpc_data_in;
					transfer_state <= LPC_TRANSFER_STATE_TR13;
				end
				LPC_TRANSFER_STATE_TR13: begin
					// Receive I/O data -- nibble 2
					rx_data[7:4] = lpc_data_in;
					transfer_state <= LPC_TRANSFER_STATE_TR14;
				end
				LPC_TRANSFER_STATE_TR14: begin
					// Wait for turn-around time to elapse
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR15;
				end
				LPC_TRANSFER_STATE_TR15: begin
					// Continue waiting for turn-around time to elapse
					transaction_complete <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR21;
				end
				LPC_TRANSFER_STATE_TR16: begin
					// Initiate abort cycle
					lpc_frame_n <= 0;
					transfer_state <= LPC_TRANSFER_STATE_TR17;
				end
				LPC_TRANSFER_STATE_TR17: begin
					// Abort cycle stage 2
					lpc_frame_n <= 0;
					transfer_state <= LPC_TRANSFER_STATE_TR18;
				end
				LPC_TRANSFER_STATE_TR18: begin
					// Abort cycle stage 3
					lpc_frame_n <= 0;
					transfer_state <= LPC_TRANSFER_STATE_TR19;
				end
				LPC_TRANSFER_STATE_TR19: begin
					// Abort cycle stage 4
					lpc_frame_n <= 0;
					lpc_data_direction <= 1;
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
					transfer_state <= LPC_TRANSFER_STATE_TR20;
				end
				LPC_TRANSFER_STATE_TR20: begin
					// Finish abort cycle and return bus to idle
					lpc_frame_n <= 1;
					lpc_data_direction <= 0;
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
					transaction_complete <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR21;
				end
				LPC_TRANSFER_STATE_TR21: begin
					// Wait for host to deassert transaction request
					if ((!tx_start_reg) && (!rx_start_reg) && (!cycle_abort_reg)) begin
						transaction_complete <= 0;
						transfer_state <= LPC_TRANSFER_STATE_IDLE;
					end
				end

				default: begin
					transfer_state <= LPC_TRANSFER_STATE_IDLE;
				end
			endcase

			lpc_reset_n <= 1;
		end
	end
endmodule
 
