# ARCHITECTURE

Aquila is a Wishbone-compatible, 32-bit, LPC slave device with 64-bit DMA support.

Aquila provides two interfaces to the system:
1. A 32-bit Wishbone slave interface with IRQ support.  All functions are supported on this interface in a CPU-interactive mode.
2. A 64-bit Wishbone master (DMA) interface, providing high speed data access and configurable DMA access protection ranges.

# USAGE

## General Usage

TODO

Usage is complex, given the nature of the protocols and overall external host involvement.  Some documentation exists in the form of working firmware for a POWER9 host system, for example here:

https://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-firmware/zephyr-firmware/-/blob/master/kestrel/src/kestrel.c

# REGISTER MAP

## [0x00 - 0x07] Device ID

 Device make/model unique identifier for PnP functionality
 Fixed value: 0x7c5250544c504353

## [0x08 - 0x0b] Device version

 Device revision (stepping)

 | Bits  | Description   |
 |-------|---------------|
 | 31:16 | Major version |
 | 15:8  | Minor version |
 | 7:0   | Patch level   |

## [0x0c - 0x0f] Control register 1

 Default: 0x00000000

 Definitions:
  - CIRQ: Interrupt request as wired to Wishbone-attached internal CPU
  - HIRQ: Interrupt request as wired to external host platform over LPC serial IRQ line

 | Bits  | Description                                                                                       |
 |-------|---------------------------------------------------------------------------------------------------|
 | 31:20 | Reserved                                                                                          |
 | 19    | Fire CIRQ on LPC I/O cycle access                                                                 |
 | 18    | Fire CIRQ on LPC TPM cycle access                                                                 |
 | 17    | Fire CIRQ on LPC firmware cycle access                                                            |
 | 16    | Enable BMC BT interface CIRQ                                                                      |
 | 15:8  | IPMI BT I/O port address                                                                          |
 | 7     | Use alternate IPMI BT HIRQ (IRQ #11) instead of standard IPMI BT HIRQ (IRQ #10)                   |
 | 6     | Enable IPMI BT host interface                                                                     |
 | 5     | Enable VUART2 host interface                                                                      |
 | 4     | Enable VUART1 host interface                                                                      |
 | 3     | Allow LPC I/O cycles from host                                                                    |
 | 2     | Allow LPC TPM cycles from host                                                                    |
 | 1     | Allow LPC firmware cycles from host                                                               |
 | 0     | Global CIRQ enable, 0 disables all CIRQs, 1 allows any enabled CIRQs to assert main LPC core CIRQ |

## [0x10 - 0x13] Control register 2

 Default: 0x00000000

 This register is used only in the CPU-interactive transfer mode.  Any activate DMA ranges will take precendence over this register for HOST firmware cycles.

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC

 | Bits  | Description                                                                                        |
 |-------|----------------------------------------------------------------------------------------------------|
 | 31:16 | Reserved                                                                                           |
 | 15:8  | LPC cycle data out (CPU to HOST)                                                                   |
 | 7:2   | Reserved                                                                                           |
 | 1     | Signal LPC bus error to HOST if asserted when bit 0 asserted                                       |
 | 0     | Assert to transfer data in bits [15:8], [1] to HOST.  Completes the active LPC cycle on assertion. |

## [0x14 - 0x17] LPC address range 1 configuration register 1

 Default: 0x00000000

 | Bits | Description                         |
 |------|-------------------------------------|
 | 31   | Enable this LPC slave address range |
 | 30   | Allow I/O cycles for this range     |
 | 29   | Allow TPM cycles for this range     |
 | 28   | Reserved                            |
 | 27:0 | LPC range start address             |

## [0x18 - 0x1b] LPC address range 1 configuration register 2

 Default: 0x00000000

 | Bits  | Description           |
 |-------|-----------------------|
 | 31:28 | Reserved              |
 | 27:0  | LPC range end address |

## [0x1c - 0x1f] LPC address range 2 configuration register 1

 Default: 0x00000000

 Same bit mapping as "LPC address range 1 configuration register 1"

## [0x20 - 0x23] LPC address range 2 configuration register 2

 Default: 0x00000000

 Same bit mapping as "LPC address range 1 configuration register 2"

## [0x24 - 0x27]  LPC address range 3 configuration register 1

 Default: 0x00000000

 Same bit mapping as "LPC address range 1 configuration register 1"

## [0x28 - 0x2b] LPC address range 3 configuration register 2

 Default: 0x00000000

 Same bit mapping as "LPC address range 1 configuration register 2"

## [0x2c - 0x2f]  LPC address range 4 configuration register 1

 Default: 0x00000000

 Same bit mapping as "LPC address range 1 configuration register 1"

## [0x30 - 0x33] LPC address range 4 configuration register 2

 Default: 0x00000000

 Same bit mapping as "LPC address range 1 configuration register 2"

## [0x34 - 0x37]  LPC address range 5 configuration register 1

 Default: 0x00000000

 Same bit mapping as "LPC address range 1 configuration register 1"

## [0x38 - 0x3b] LPC address range 5 configuration register 2

 Default: 0x00000000

 Same bit mapping as "LPC address range 1 configuration register 2"

## [0x3c - 0x3f]  LPC address range 6 configuration register 1

 Default: 0x00000000

 Same bit mapping as "LPC address range 1 configuration register 1"

## [0x40 - 0x43] LPC address range 6 configuration register 2

 Default: 0x00000000

 Same bit mapping as "LPC address range 1 configuration register 2"

## [0x44 - 0x47] DMA configuration register 1

 Default: 0x00000000

 | Bits | Description                                                                                                               |
 |------|---------------------------------------------------------------------------------------------------------------------------|
 | 31:8 | Reserved                                                                                                                  |
 | 7:4  | LPC IDSEL filter                                                                                                          |
 | 3    | Reserved                                                                                                                  |
 | 2    | IDSEL filter enable.  When asserted, the DMA engine will require the LPC IDSEL to match the configured filter IDSEL value |
 | 1    | Enable DMA for LPC firmware write cycles                                                                                  |
 | 0    | Enable DMA for LPC firmware read cycles                                                                                   |

## [0x48 - 0x4b] DMA configuration register 2

 Default: 0x00000000

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC

 | Bits | Description                        |
 |------|------------------------------------|
 | 31:0 | CPU DMA window base address [31:0] |

 NOTE: The DMA engine only supports full word length (64 bit) CPU bus alignment, therefore bits [3:0] of this register are hardwired to zero.

## [0x4c - 0x4f] DMA configuration register 3

 Default: 0x00000000

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC

 | Bits | Description                         |
 |------|-------------------------------------|
 | 31:0 | CPU DMA window base address [63:32] |

## [0x50 - 0x53] DMA configuration register 4

 Default: 0x00000000

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC

 | Bits | Description                        |
 |------|------------------------------------|
 | 31:0 | LPC firmware window length (bytes) |

 NOTE: The DMA engine only supports full word length (64 bit) CPU bus alignment, therefore bits [3:0] of this register are hardwired to zero.

## [0x54 - 0x57] DMA configuration register 5

 Default: 0x00000000

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC

 | Bits | Description                              |
 |------|------------------------------------------|
 | 31:0 | LPC firmware window start offset (bytes) |

 This register defines the start address (DMA window offset) of the active LPC firmware access window.

 All LPC firmware transfers start with an implicit LPC base address of 0x0, which corresponds to offset 0x0 in the configured CPU DMA window (see "DMA configuration register 2").
 This register allows remapping of the LPC base address within the CPU DMA window, thus allowing LPC address 0x0 to be placed anywhere within the configured CPU DMA memory region.  In effect, it is the offset into DMA memory space where the LPC memory space origin is placed.

 Together with the "DMA configuration register 6" register, a defined region of LPC firmware memory space can be set up for DMA access, which is then mapped onto an equivalent region of CPU DMA memory.

## [0x58 - 0x5b] DMA configuration register 6

 Default: 0x00000000

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC

 | Bits | Description                            |
 |------|----------------------------------------|
 | 31:0 | LPC firmware window end offset (bytes) |

 This register defines the end address of the active LPC firmware access window.

 Together with the "DMA configuration register 5" register, a defined region of LPC firmware memory space can be set up for DMA access, which is then mapped onto an equivalent region of CPU DMA memory.

## [0x5c - 0x5f] DMA configuration register 7

 Default: 0x00000000

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC

 | Bits | Description               |
 |------|---------------------------|
 | 31:0 | LPC firmware address mask |

 This register defines the mask applied to all inbound LPC firmware space addresses, prior to any mapping of those addresses into the DMA region.

 This design allows a specific section of CPU DMA memory to be effectively replicated through the entire LPC address space.  In particular, it helps to ensure the DMA window data is available at the end of the LPC firmware address space, as expected by various HOST access patterns.

## [0x60 - 0x63] Status register 1

 Default: 0x00000000

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC

 | Bits  | Description                                                                  |
 |-------|------------------------------------------------------------------------------|
 | 31:24 | Reserved                                                                     |
 | 23:20 | IDSEL of pending LPC firmware cycle                                          |
 | 19:16 | MSIZE of pending LPC firmware cycle                                          |
 | 15:5  | Reserved                                                                     |
 | 4     | Asserted when LPC bus is in external HOST-driven reset                       |
 | 3:2   | LPC cycle type from host -- 0 == I/O, 1 == TPM, 2 == firmware, 3 == reserved |
 | 1     | LPC cycle direction from HOST -- 0 == read, 1 == write                       |
 | 0     | Attention flag from LPC core                                                 |

## [0x64 - 0x67] Status register 2

 Default: 0x00000000

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC

 | Bits  | Description                  |
 |-------|------------------------------|
 | 31:28 | Reserved                     |
 | 27:0  | Address of pending LPC cycle |

 This register contains the target LPC address of any pending LPC transaction initiated by the HOST.

## [0x68 - 0x6b] Status register 3

 Default: 0x00000000

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC

 | Bits | Description                             |
 |------|-----------------------------------------|
 | 31:8 | Reserved                                |
 | 7:0  | HOST-provided data of pending LPC cycle |

 This register contains the HOST-provided data of any pending LPC transaction initiated by the HOST.

 The contents of this register are only defined when the LPC cycle type is WRITE; the contents are undefined for all other cycle types.

## [0x6c - 0x6f] Status register 4

 Default: 0x00000000

 Definitions:
  - CPU:  Wishbone-attached internal CPU
  - HOST: External host platform attached via LPC
  - CIRQ: Interrupt request as wired to Wishbone-attached internal CPU
  - HIRQ: Interrupt request as wired to external host platform over LPC serial IRQ line

 | Bits  | Description                                                                                      |
 |-------|--------------------------------------------------------------------------------------------------|
 | 31:12 | Reserved                                                                                         |
 | 11:10 | Reason for VUART2 IRQ assert -- 0 == undefined, 1 == queue threshold reached, 2 == queue timeout |
 | 9:8   | Reason for VUART1 IRQ assert -- 0 == undefined, 1 == queue threshold reached, 2 == queue timeout |
 | 7     | Reserved                                                                                         |
 | 6     | LPC I/O cycle CIRQ asserted                                                                      |
 | 5     | LPC TPM cycle CIRQ asserted                                                                      |
 | 4     | LPC firmware cycle CIRQ asserted                                                                 |
 | 3     | IPMI BT CIRQ asserted                                                                            |
 | 2     | VUART2 CIRQ asserted                                                                             |
 | 1     | VUART1 CIRQ asserted                                                                             |
 | 0     | LPC global CIRQ asserted                                                                         |

## [0x70 - 0x73] IPMI BT control register

 Default: 0x00000000

 | Bits | Description |
 |------|-------------|
 | 31:8 | Reserved    |
 | 7    | B_BUSY      |
 | 6    | H_BUSY      |
 | 5    | OEM0        |
 | 4    | EVT_ATN     |
 | 3    | B2H_ATN     |
 | 2    | H2B_ATN     |
 | 1    | CLR_RD_PTR  |
 | 0    | CLR_WR_PTR  |

 This is the IPMI-defined BMC-side (CPU accessible) BT control register (BT_CTRL).

 Please refer to the IPMI specification [1] for a full register definition.

# LICENSE

Aquila is licensed under the terms of the GNU LGPLv3+ (LPC slave) and the GNU GPL v3 (LPC master), with included third party components licensed under Apache 2.0.  See the individual RTL files for specific license information.

# DOCUMENTATION CREDITS

(c) 2022 Raptor Engineering, LLC

# REFERENCES

1. https://www.intel.com/content/dam/www/public/us/en/documents/specification-updates/ipmi-intelligent-platform-mgt-interface-spec-2nd-gen-v2-0-spec-update.pdf
